import { Fragment } from 'react';
import { Button } from 'react-bootstrap';

export default function Home(){
	return (
		<Fragment>
		<div className="p-5 ">
			<h1>Hello!</h1>
			<p>
			  This is a simple hero unit, a simple jumbotron-style component for calling
			  extra attention to featured content or information.
			</p>
			<p>
			  <Button variant="primary">Learn more</Button>
			</p>
		</div>
		  
		</Fragment>
	)
}
