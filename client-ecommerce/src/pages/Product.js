import { Fragment, useEffect, useState, useContext } from 'react';
import Filter from '../components/Filter'
import ProductCard from '../components/ProductCard';
import { Button, Row, Col, Card, Container } from 'react-bootstrap';
import Cart from '../components/Cart';
import UserContext from '../UserContext';
import UserProvider from '../UserContext';



// export default function Product(){

// 	return (
// 		<Fragment>
// 			<Filter />
// 			<Highlights />	
// 		</Fragment>
// 	);
// }


export default function Product(){

	const {user, setUser, forceRender, setForceRender } = useContext(UserContext);	
	const [products, setProducts] = useState([]);
	const [tempArray, setTempArray] = useState(JSON.parse(localStorage.getItem('cart')))

	useEffect(() => {

		setForceRender(forceRender + 1);
		setForceRender(forceRender - 1);
	}, [tempArray])

	console.log(tempArray)


	useEffect(() => {
		fetch('http://localhost:4000/products/all', {
			headers: {
				Authorization: `Bearer ${ user.token }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			
				setProducts(
					data.map(product => {
						return (
							
							<ProductCard key={product.id} tempArray={tempArray} setTempArray={setTempArray} productProp = {product} />
						)
					})
				);
			
			
		})
	}, [])
 


	return (
		<Fragment>
			{
				<Fragment>
					<div className="text-md-left text-center d-flex">
						<h1 className="mt-5">Products</h1>
						<div className="ml-auto align-self-end">
							<div className="pr-5 pb-3">
								<Cart setTempArray={setTempArray} tempArray={tempArray} />
							</div>
						</div>
					</div>
					<Filter className="" />
					<Row className="mt-3 mb-3 card-deck">
						{products}
					</Row>	
				</Fragment>
			}
		</Fragment>
		
		
	)
}
