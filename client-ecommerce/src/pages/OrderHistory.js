import { Fragment, useEffect, useState, useContext } from 'react';
import { Button, Row, Col, Card, Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import OrderTicket from '../components/OrderTicket';
import Filter from '../components/Filter';



export default function OrderHistory(prop){

	const {user, setUser} = useContext(UserContext);
	const [ orderTickets, setOrderTickets ]  = useState([]);

	let endpoint = 'myOrders';

	if(prop.end !== undefined){
		endpoint = prop.end;
	}


	useEffect(() => {
		fetch(`http://localhost:4000/users/${endpoint}`, {
			headers: {
				Authorization: `Bearer ${ user.token }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			data.reverse();
				setOrderTickets(

					data.map(order => {
						return (
							<OrderTicket key={order.id} orders={order} items={order.items} />
						)
					})
				);
			
			
		})
	}, []) 

	return (
		<Fragment>

			{
				(endpoint == 'myOrders') ?
					<Fragment>
						<div>
							<h1 className="mt-5">Order History</h1>
						</div>
						<div>
							<Filter />
						</div>
						<Row className="">
							{orderTickets}
						</Row>
					</Fragment>
				:
					<Fragment>
						<div>
							<Filter />
						</div>
						<Row className="">
							{orderTickets}
						</Row>
					</Fragment>
			}	
			
		</Fragment>
		
		
	)
} 
