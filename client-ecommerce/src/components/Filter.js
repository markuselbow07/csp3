import Fragment from 'react';
import { Button, Row, Col, Form, InputGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Filter({data}){

	return (
		<Row>
			<Col className="p-5"> 
					
				<InputGroup className="mb-3 col-md-6">
				    <Form.Control
				      placeholder="Filter"
				      aria-label="Username"
				      aria-describedby="basic-addon1"
				    />
				</InputGroup>
					
			</Col>
		</Row>
	)
}
